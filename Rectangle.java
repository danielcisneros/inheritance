/**
 * A rectangle class to act as a child for the shape class.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 */

public class Rectangle extends Shape{
    protected int area = 20;

    //Setters
    public void setArea(int myInputArea) {
        area = myInputArea;
    }

    //Getters
    public int getArea() {
        return area;
    }
}