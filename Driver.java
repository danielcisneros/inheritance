/**
 * A driver class for the shape class and it's children.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 */

public class Driver {
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();

        //Prints information from the parent class using a parent method.
        System.out.println(myRect.toString());

        //Prints information from the child class.
        System.out.println(myRect.getArea());
    }
}