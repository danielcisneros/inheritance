/**
 * This is a basic class to hold shapes for our inheritance demo.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 */

public class Shape {
    protected int base = 4;
    protected int height = 5;

    //Setters
    public void setBase(int inputBase) {
        base = inputBase;
    }

    public void setHeight(int inputHeight) {
        height = inputHeight;
    }

    //Getters
    public int getBase() {
        return base;
    }

    public int getHeight() {
        return height;
    }

    //Simple String Output
    public String toString() {
        return "Your base is " + base + "\nYour height is " + height;
    }
}